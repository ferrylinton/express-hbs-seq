"use strict";

const { Sequelize, DataTypes } = require("sequelize");

const tableName = 'sec_role_authority';


module.exports = {
  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, {

      role_id: {
        type: DataTypes.STRING(11),
        allowNull: false
      },

      authority_id: {
        type: DataTypes.STRING(11),
        allowNull: false
      },

      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      },

      created_by: {
        type: DataTypes.STRING(70),
        allowNull: false,
        defaultValue: process.env.DEFAULT_USER
      }

    });

    await queryInterface.addConstraint(tableName, {
      fields: ['role_id', 'authority_id'],
      type: 'primary key',
      name: 'sec_role_authority_pk'
    });

    await queryInterface.addConstraint(tableName, {
      type: 'foreign key',
      fields: ['role_id'],
      name: 'sec_role_authority_role_id_fk',
      references: {
        table: 'sec_role',
        field: 'id'
      }
    });

    await queryInterface.addConstraint(tableName, {
      type: 'foreign key',
      fields: ['authority_id'],
      name: 'sec_role_authority_authority_id_fk',
      references: {
        table: 'sec_authority',
        field: 'id'
      }
    });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }
  
};