"use strict";

const { Sequelize, DataTypes } = require("sequelize");
const tableName = 'sec_revoked_token';


module.exports = {

  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, {

      id: {
        type: DataTypes.STRING(50),
        allowNull: false,
        primaryKey: true,
      },

      token: {
        type: DataTypes.TEXT,
        allowNull: false
      },

      username: {
        type: DataTypes.STRING(50),
        allowNull: false
      },

      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      }

    });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }

};