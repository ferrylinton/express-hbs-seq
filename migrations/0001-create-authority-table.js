"use strict";

const { Sequelize, DataTypes } = require("sequelize");
const tableName = "sec_authority";


module.exports = {

  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, {

      id: {
        type: DataTypes.STRING(11),
        allowNull: false,
        primaryKey: true
      },

      name: {
        type: DataTypes.STRING(50),
        allowNull: false
      },

      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      },

      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      },

      created_by: {
        type: DataTypes.STRING(70),
        allowNull: false,
        defaultValue: process.env.DEFAULT_USER
      },

      updated_by: {
        type: DataTypes.STRING(70),
        allowNull: false,
        defaultValue: process.env.DEFAULT_USER
      }

    });

    await queryInterface.addConstraint(tableName, {
      type: "unique",
      fields: ["name"],
      name: "sec_authority_name_uq"
    });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }

};