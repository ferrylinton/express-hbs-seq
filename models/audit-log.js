"use strict";

const { Sequelize, Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {

  class AuditLog extends Model {

    static associate(models) {
      // define association here
    }

  };

  AuditLog.init({

    id: {
      type: DataTypes.STRING(11),
      primaryKey: true
    },

    actionStatus: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },

    actionType: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },

    dataId: {
      type: DataTypes.STRING(11),
      allowNull: true
    },

    actionModel: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },

    requestBody: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    result: {
      type: DataTypes.TEXT,
      allowNull: true
    },

    dataHash: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: '-'
    },
    
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn("now")
    },

    createdBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    }

  }, {
    sequelize,
    modelName: 'AuditLog',
    tableName: 'audit_log'
  });

  return AuditLog;

};