"use strict";

const { Sequelize, Model } = require("sequelize");
const { Role } = require('./index');
const { uid } = require("uid");

module.exports = (sequelize, DataTypes) => {

  class User extends Model {

    static associate(models) {
      models.User.belongsTo(models.Role, { as: 'role' });

      models.User.addScope('withRole', {
        include: [
          {
            model: models.Role, as: 'role'
          }
        ]
      });
    }

  };

  User.init({

    id: {
      type: DataTypes.STRING(11),
      primaryKey: true
    },

    activated: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },

    locked: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },

    loginAttemptCount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },

    username: {
      type: DataTypes.STRING(50),
      allowNull: false
    },

    email: {
      type: DataTypes.STRING(50),
      allowNull: false
    },

    passwordHash: {
      type: DataTypes.STRING(250),
      allowNull: false
    },

    createdAt: {
      type: DataTypes.DATE
    },

    updatedAt: {
      type: DataTypes.DATE
    },

    createdBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    },

    updatedBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    }

  }, {
    sequelize,
    modelName: 'User',
    tableName: 'sec_user',
    hooks: {

      beforeCreate: (instance, options) => {
        let user = options.user.id + "," + options.user.username;
        let now = new Date();

        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;
        instance.createdBy = user;
        instance.updatedBy = user;
      },

      beforeUpdate: (instance, options) => {
        instance.updatedAt = new Date();
        instance.updatedBy = options.user.id + "," + options.user.username;
      }

    }
  });

  return User;

};