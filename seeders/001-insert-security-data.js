"use strict";

const { QueryTypes } = require("sequelize");
const { uid } = require("uid");

const passwordHash = "$2a$10$0FWZqemfw0I7SxVc1Hpnfu7SMrQpbB3W9N0.0qFvt5dwX0q.hqdoS";

module.exports = {
  up: async (queryInterface) => {
    let authorities = [
      { id: uid(), name: "DEFAULT_USER" },
      { id: uid(), name: "AUTHORITY_VIEW" },
      { id: uid(), name: "AUTHORITY_MODIFY" },
      { id: uid(), name: "ROLE_VIEW" },
      { id: uid(), name: "ROLE_MODIFY" },
      { id: uid(), name: "USER_VIEW" },
      { id: uid(), name: "USER_MODIFY" }
    ];

    let roles = [
      { id: uid(), name: "Admin" },
      { id: uid(), name: "User" }
    ];

    // Add Admin"s athorities

    await queryInterface.bulkInsert("sec_authority", authorities, {});
    await queryInterface.bulkInsert("sec_role", roles, {});

    var authorityIds = await queryInterface.sequelize.query("SELECT id from sec_authority", { type: QueryTypes.SELECT });
    var adminIds = await queryInterface.sequelize.query("SELECT id from sec_role where name = 'Admin'", { type: QueryTypes.SELECT });

    let roleAuthorities = [];
    for (let j = 0; j < authorityIds.length; j++) {
      roleAuthorities.push({
        role_id: adminIds[0].id,
        authority_id: authorityIds[j].id
      });
    }

    await queryInterface.bulkInsert("sec_role_authority", roleAuthorities, {});

    // Add User"s athorities

    authorityIds = await queryInterface.sequelize.query("SELECT id from sec_authority as auth where name like '%VIEW' or name = 'DEFAULT_USER'", { type: QueryTypes.SELECT });
    var userIds = await queryInterface.sequelize.query("SELECT id from sec_role where name = 'User'", { type: QueryTypes.SELECT });

    roleAuthorities = [];
    for (let j = 0; j < authorityIds.length; j++) {
      roleAuthorities.push({
        role_id: userIds[0].id,
        authority_id: authorityIds[j].id
      });
    }

    await queryInterface.bulkInsert("sec_role_authority", roleAuthorities, {});

    let users = [
      {
        id: uid(),
        username: "admin01",
        email: "admin01@yopmail.com",
        password_hash: passwordHash,
        role_id: adminIds[0].id
      },
      {
        id: uid(),
        username: "user01",
        email: "user01@yopmail.com",
        password_hash: passwordHash,
        role_id: userIds[0].id
      }
    ]

    await queryInterface.bulkInsert("sec_user", users, {});


    authorities = []
    for (let j = 10; j < 100; j++) {
      authorities.push({
        id: uid(),
        name: "authority_" + j
      });
    }
    await queryInterface.bulkInsert("sec_authority", authorities, {});

  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete("sec_role_authority", null, {});
    await queryInterface.bulkDelete("sec_role", null, {});
    await queryInterface.bulkDelete("sec_authority", null, {});
    await queryInterface.bulkDelete("sec_user", null, {});
  }
};


