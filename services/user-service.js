const pageUtil = require('../util/page-util');
const auditLogService = require("../services/audit-log-service");
const Sequelize = require('sequelize');
const NotFoundError = require('../error/not-found-error');
const bcrypt = require('bcryptjs');
const { USER } = require("../config/constant").ActionModel;
const { CREATE, UPDATE, DELETE } = require("../config/constant").ActionType;
const { Op } = require('sequelize');
const { User } = require('../models/index');


async function findAndCountAll(req) {
  let params = pageUtil.getParams(req);
  params.distinct = true;
  params.col = "User.id";
  params.order = [
    ['username', 'ASC'],
  ]

  if (params.keyword) {
    let keyword = "%" + params.keyword.toLowerCase() + "%";

    params.where = {
      [Op.or]: [
        Sequelize.where(Sequelize.fn('lower', Sequelize.col('User.username')), { [Op.like]: keyword }),
        Sequelize.where(Sequelize.fn('lower', Sequelize.col('User.created_by')), { [Op.like]: keyword }),
        Sequelize.where(Sequelize.fn('lower', Sequelize.col('User.updated_by')), { [Op.like]: keyword }),
      ]
    }
  }

  let data = await User.scope('withRole').findAndCountAll(params);
  let page = pageUtil.getPage(req, data.count);
  return { data, page };
}

async function findOne(req) {
  const user = await User.findOne({
    where: { id: req.params.id }
  });

  if (user) {
    return user;
  } else {
    var url = req.protocol + "://" + req.headers.host + req.originalUrl;
    throw new NotFoundError(`['${url}'] is not found`);
  }
}

async function create(req) {
  try {
    let { username, email, roleId } = req.body;
    let passwordHash = bcrypt.hashSync(req.body.password, 10);

    let user = await User.create({ username, email, passwordHash, roleId }, { user: req.user });
    await auditLogService.log(USER, CREATE, true, user, req);
    return user;
  } catch (err) {
    await auditLogService.log(USER, CREATE, false, err, req);
    throw err;
  }
}

async function update(req) {
  try {
    let user = await findOne(req);
    user.username = req.body.username;
    user.email = req.body.email;
    user.roleId = req.body.roleId;

    await user.save({ user: req.user });
    await auditLogService.log(USER, UPDATE, true, user, req);
    return user;
  } catch (err) {
    console.log(err);
    await auditLogService.log(USER, UPDATE, false, err, req);
    throw err;
  }
}

async function destroy(req) {
  try {
    user = await findOne(req);
    await user.destroy();
    await auditLogService.log(USER, DELETE, true, user, req);
  } catch (err) {
    await auditLogService.log(USER, DELETE, false, err, req);
    throw err;
  }
}

async function isUsernameExist(value, obj) {
  const user = await User.findOne({
    where: { username: value }
  });

  if (user) {
    if (!(obj.req.params.id && (obj.req.params.id == user.id))) {
      return Promise.reject(`['${value}'] is already exist`);
    }
  }
}

async function isEmailExist(value, obj) {
  const user = await User.findOne({
    where: { email: value }
  });

  if (user) {
    if (!(obj.req.params.id && (obj.req.params.id == user.id))) {
      return Promise.reject(`['${value}'] is already exist`);
    }
  }
}

module.exports = {
  findAndCountAll: findAndCountAll,
  findOne: findOne,
  create: create,
  update: update,
  destroy: destroy,
  isUsernameExist: isUsernameExist,
  isEmailExist: isEmailExist
};
