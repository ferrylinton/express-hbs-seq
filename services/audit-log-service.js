const md5 = require("crypto-js/md5");
const logger = require('../config/winston');
const { ActionStatus } = require("../config/constant");
const { uid } = require("uid");
const { AuditLog } = require("../models/index");

const EXCLUDE = ["password", "passwordConfirmation"]

function init(actionModel, actionType, actionStatus, req) {
    deleteProtectedAttributes(req);

    let auditLog = {};
    auditLog.id = uid();
    auditLog.actionModel = actionModel;
    auditLog.actionType = actionType;
    auditLog.createdAt = new Date();
    auditLog.createdBy = req.user.id + "," + req.user.username;
    auditLog.requestBody = JSON.stringify(req.body);
    auditLog.actionStatus = actionStatus ? ActionStatus.SUCCESS : ActionStatus.ERROR;
    return auditLog;
}

function deleteProtectedAttributes(req){
    EXCLUDE.forEach(function(attr) {
        delete req.body[attr];
    });
}

async function log(actionModel, actionType, actionStatus, result, req) {
    try {
        let auditLog = init(actionModel, actionType, actionStatus, req);

        if (actionStatus) {
            auditLog.result = JSON.stringify(result);
            auditLog.dataId = (result) ? result.id : "0";
        } else {
            auditLog.result = (result.original) ? result.original.sqlMessage : result.message;
        }

        auditLog.dataHash = md5(JSON.stringify(auditLog)).toString();
        await AuditLog.create(auditLog);
    } catch (err) {
        logger.error(err.stack);
    }
}


module.exports = {
    log
};